<?php

//Ruta para la obtencion de el inventario 
Route::get('bodega','BodegaController@index')->name('bodega.index');

//Rutas para el manejo de las ordenes
Route::resource('/','OrdenController')->only('index','create');

//Ruta para las estadisticas
Route::get('/estadisticas','EstadisticaController@index')->name('estadisticas.index');

