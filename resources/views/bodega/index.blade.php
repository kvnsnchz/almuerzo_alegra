@extends('templates.navegacion')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class="label label-title">
      BODEGA
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title label label-primary">INGREDIENTES</h3>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th>#</th>
                  <th class="th-large">NOMBRE</th>
                  <th>DISPONIBLE</th>
                </tr>
              </thead>
              @if ($ingredientes->isEmpty())
                <tr class="table-row">
                  <td></td>
                  <td>NO HAY INGREDIENTES</td>
                </tr>
              @else
                @foreach ($ingredientes as $ing)
                  <tr class="table-row">
                    <td>{{$ing->id}}</td>
                    <td>{{$ing->nombre}}</td>
                    <td class="td-center">{{$ing->cantidad_disp}}</td>
                  </tr>
                @endforeach
              @endif
            </table>
          </div><!-- /.box-body -->
          <div class="box-footer clearfix box-footer-dark">
          </div>
        </div><!-- /.box -->
      </div>
      <div class="col-md-6">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title label label-primary">COMPRAS</h3>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th>#</th>
                  <th class="th-medium">INGREDIENTE</th>
                  <th>CANTIDAD</th>
                  <th></th>
                </tr>
              </thead>
              @if ($compras->isEmpty())
                <tr class="table-row">
                  <td></td>
                  <td>NO HAY COMPRAS</td>
                </tr>
              @else
                @foreach ($compras as $comp)
                  <tr class="table-row">
                    <td>{{$comp->id}}</td>
                    <td>{{$comp->ingrediente->nombre}}</td>
                    <td class="td-center">{{$comp->cantidad}}</td>
                    <td>
                      <span class="label label-hour">{{$comp->diff_fecha}}</span>
                    </td>
                  </tr>
                @endforeach
              @endif
            </table>
          </div><!-- /.box-body -->
          <div class="box-footer clearfix box-footer-dark">
              {{$compras->links('templates.paginate')}}
          </div>
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
@endsection