@extends('templates.navegacion')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class="label label-title">
      ESTADISTICAS
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">COMPRAS POR INGREDIENTE</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
            @if($sin_compras)
              <p>SIN COMPRAS AUN</p>
            @else
              <canvas id="pieChart2" style="height:250px"></canvas>
            @endif
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
      <div class="col-md-6">
        <!-- DONUT CHART -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">ORDENES POR RECETA</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
            @if($sin_ordenes)
              <p>SIN ORDENES AUN</p>
            @else
              <canvas id="pieChart1" style="height:250px"></canvas>
            @endif
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
@endsection

@section('scripts')
  <script src="plugins/chartjs/Chart.min.js"></script>
  <script>
    $(function () {
        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */

        //-------------
        //- ORDENES POR RECETAS -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var sin_ordenes = <?php echo json_encode($sin_ordenes)?>;
        if(!sin_ordenes){
          var pieChartCanvas = $("#pieChart1").get(0).getContext("2d");
          var pieChart = new Chart(pieChartCanvas);
          var footer = $("#box-foote1");
          recetas = <?php echo json_encode($recetas)?>;
          n_ordenes = <?php echo json_encode($n_ordenes)?>;
          var data= [];
          for (var i = 0; i < recetas.length; i++) {
            data[i] = [];
            data[i]['label'] = recetas[i]['nombre'].toUpperCase();
            data[i]['value'] = n_ordenes[i];
          }
          var PieData = [
            {
              value: data[0]['value'],
              color: "#f56954",
              highlight: "#f56954",
              label: data[0]['label'],
            },
            {
              value: data[1]['value'],
              color: "#00a65a",
              highlight: "#00a65a",
              label: data[1]['label']
            },
            {
              value: data[2]['value'],
              color: "#f39c12",
              highlight: "#f39c12",
              label: data[2]['label']
            },
            {
              value: data[3]['value'],
              color: "#00c0ef",
              highlight: "#00c0ef",
              label: data[3]['label']
            },
            {
              value: data[4]['value'],
              color: "#3c8dbc",
              highlight: "#3c8dbc",
              label: data[4]['label']
            },
            {
              value: data[5]['value'],
              color: "#d2d6de",
              highlight: "#d2d6de",
              label: data[5]['label']
            }
          ];
          var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: "#fff",
            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: "easeOutBounce",
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
           
          };
          //Create pie or douhnut chart
          // You can switch between pie and douhnut using the method below.

          pieChart.Doughnut(PieData, pieOptions);
      }
      
      //-------------
      //- COMPRAS POR INGREDIENTE -
      //-------------
      // Get context with jQuery - using jQuery's .get() method.
      sin_compras = <?php echo json_encode($sin_compras)?>;
      if(!sin_compras){
        var pieChartCanvas = $("#pieChart2").get(0).getContext("2d");
        var pieChart = new Chart(pieChartCanvas);
        ingredientes = <?php echo json_encode($ingredientes)?>;
        n_compras = <?php echo json_encode($n_compras)?>;
        var data= [];
        for (var i = 0; i < ingredientes.length; i++) {
          data[i] = [];
          data[i]['label'] = ingredientes[i]['nombre'].toUpperCase();
          data[i]['value'] = n_compras[i];
        }
        var PieData = [
          {
            value: data[0]['value'],
            color: "#f56954",
            highlight: "#f56954",
            label: data[0]['label'],
          },
          {
            value: data[1]['value'],
            color: "#00a65a",
            highlight: "#00a65a",
            label: data[1]['label']
          },
          {
            value: data[2]['value'],
            color: "#f39c12",
            highlight: "#f39c12",
            label: data[2]['label']
          },
          {
            value: data[3]['value'],
            color: "#00c0ef",
            highlight: "#00c0ef",
            label: data[3]['label']
          },
          {
            value: data[4]['value'],
            color: "#3c8dbc",
            highlight: "#3c8dbc",
            label: data[4]['label']
          },
          {
            value: data[5]['value'],
            color: "#d2d6de",
            highlight: "#d2d6de",
            label: data[5]['label']
          },
          {
            value: data[6]['value'],
            color: "#f45c30",
            highlight: "#f45c30",
            label: data[6]['label']
          },
          {
            value: data[7]['value'],
            color: "#a0c0ff",
            highlight: "#a0c0ff",
            label: data[7]['label']
          },
          {
            value: data[8]['value'],
            color: "#ad8dcc",
            highlight: "#ad8dcc",
            label: data[8]['label']
          },
          {
            value: data[9]['value'],
            color: "#e2d6ee",
            highlight: "#e2d6ee",
            label: data[9]['label']
          }
        ];
        var pieOptions = {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke: true,
          //String - The colour of each segment stroke
          segmentStrokeColor: "#fff",
          //Number - The width of each segment stroke
          segmentStrokeWidth: 2,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 50, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps: 100,
          //String - Animation easing effect
          animationEasing: "easeOutBounce",
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate: true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale: false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);
      }
    });
  </script>
@endsection