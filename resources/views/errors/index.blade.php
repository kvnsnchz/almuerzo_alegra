@extends('templates.navegacion')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class="label label-danger">
      ERROR
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
  	<h2><i class="glyphicon glyphicon-warning-sign"></i> 
  		¡HA OCURRIDO UN ERROR!
  	</h2>
    <p>Estamos trabajando para solucionarlo. Intentalo más tarde.</p>
  </section><!-- /.content -->
@endsection