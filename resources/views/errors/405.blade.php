@extends('templates.navegacion')

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class="label label-warning">
      403 ERROR
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
  	<h2><i class="glyphicon glyphicon-ban-circle"></i> 
  		¡NO ESTAS AUTORIZADO, HA ESTAR AQUI!
  	</h2>
  </section><!-- /.content -->
@endsection