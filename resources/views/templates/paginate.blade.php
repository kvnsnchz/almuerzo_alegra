@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link" aria-hidden="true">&lsaquo;</span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @if(count($element) > 5)
                    @for ($i = 1;$i < 4;$i++)
                        @if($paginator->currentPage() < 4 || $i == 1)
                            @if ($i == $paginator->currentPage())
                                <li class="page-item active" aria-current="page"><span class="page-link">{{ $i }}</span></li>
                            @else
                                <li class="page-item"><a class="page-link" href="{{$element[$i]}}">{{ $i}}</a></li>
                            @endif
                            @if($paginator->currentPage() >= 4)
                                <li class="page-item"><span class="page-link">...</span></li>
                            @endif
                        @endif
                    @endfor
                    @if($paginator->currentPage() >= 4 && count($element)-$paginator->currentPage() >=3)
                         <li class="page-item active" aria-current="page"><span class="page-link">{{ $paginator->currentPage() }}</span></li>
                    @endif
                    @if(count($element)-$paginator->currentPage() >= 3)
                        <li class="page-item"><span class="page-link">...</span></li>
                    @endif
                    @for ($i = (count($element)-2);$i <= count($element);$i++)
                        @if(count($element)-$paginator->currentPage() < 3 || $i == count($element))
                            @if ($i == $paginator->currentPage())
                                <li class="page-item active" aria-current="page"><span class="page-link">{{ $i }}</span></li>
                            @else
                                <li class="page-item"><a class="page-link" href="{{$element[$i]}}">{{ $i }}</a></li>
                            @endif
                        @endif
                    @endfor
                @else
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
                
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
            </li>
        @else
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">&rsaquo;</span>
            </li>
        @endif
    </ul>
@endif