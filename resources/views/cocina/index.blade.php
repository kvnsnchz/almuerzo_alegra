@extends('templates.navegacion')

@section('head')
  <style type="text/css">
    .box-tools-flex{
      display: flex;
      flex-direction: row;
      align-items: center;
    }
    .box-tools-flex > a{
      margin-right: 20px;
      font-size: 20px;
      margin-top: 5px;
      color: rgb(0, 141, 76);
    }
    .box-tools-flex > a:hover{
      color: rgb(57, 132, 57);
    }
  </style>
@endsection

@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class="label label-title">
      Cocina
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title label label-primary">ORDENES</h3>
            	<div class="box-tools box-tools-flex">
                
                    <a href="{{route('index')}}"><i class="glyphicon glyphicon-refresh"></i></a>
              
                    <button class="btn btn-success btn-block btn-sm" onclick="window.location='{{route('create')}}'">
                      <smart>NUEVA ORDEN</smart>
                    </button>
		           
		    	    </div>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th>#</th>
                  <th class="th-medium">RECETA</th>
                  <th>ESTADO</th>
                  <th>SOLICITADA</th>
                </tr>
              </thead>
              @if ($ordenes->isEmpty())
                <tr class="table-row">
                  <td></td>
                  <td>NO HAY ORDENES</td>
                </tr>
              @else
	              @foreach ($ordenes as $ord)
	                <tr class="table-row">
	                  <td>{{$ord->id}}</td>
	                  <td>{{$ord->receta->nombre}}</td>
	                  <td>
                      @if ($ord->estado->id == 2)
                        <span class="label label-primary">
                          {{$ord->estado->nombre}}
                        </span>
                      @else
                        <span class="label label-warning">
                          {{$ord->estado->nombre}}
                        </span>
                      @endif
                    </td>
	                  <td>
                       <span class="label label-hour">{{$ord->diff_fecha}}</span>
                    </td>
	                </tr>
	              @endforeach
	          @endif
            </table>
          </div><!-- /.box-body -->
          <div class="box-footer clearfix box-footer-dark">
              {{$ordenes->links('templates.paginate')}}
          </div>
        </div><!-- /.box -->
      </div>
      <div class="col-md-6">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title label label-primary">RECETAS</h3>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table">
              @if ($recetas->isEmpty())
                <tr class="table-row">
                  <td>NO HAY RECETAS</td>
                </tr>
              @else
                @foreach ($recetas as $rec)
                  <tr class="table-row">
                    <div class="box box-default collapsed-box">
                      <div class="box-header with-border">
                        <h5 class="box-title box-title-upper" id="box-title">{{$rec->nombre}}</h5>
                        <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse">
                          @if (count($rec->receta_ingrediente) == 1)
                            <span class="label label-success" id="button-view">
                              ver el ingrediente <i class="fa fa-plus"></i>
                            </span>
                          @else
                            <span class="label label-success" id="button-view">
                              ver los {{count($rec->receta_ingrediente)}} ingredientes
                            </span>
                          @endif
                        </button>
                      </div><!-- /.box-tools -->
                      </div>
                      <div class="box-body box-body-solid">
                        <ul>
                          @foreach ($rec->receta_ingrediente as $ri)
                            @if ($ri->cantidad == 1)
                              <li>
                                {{$ri->cantidad}} unidad de {{$ri->ingrediente->nombre}}
                              </li>
                            @else
                              <li>
                                {{$ri->cantidad}} unidades de {{$ri->ingrediente->nombre}}
                              </li>
                            @endif
                          @endforeach
                        </ul>
                      </div><!-- /.box-body -->
                    </div>  
                    
                  </tr>
                @endforeach
              @endif
            </table>
          </div><!-- /.box-body -->
          <div class="box-footer clearfix box-footer-dark">
          </div>
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
@endsection