<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
* Modelo de la tabla Estados
*/
class Estado extends Model
{
  protected $table = "ESTADOS";
  protected $fillable = array('nombre');
  protected $guarded = array('id');
  public $timestamps = false;
  
  public function orden(){
     return $this->hasMany('App\Orden','id_estado','id');
  }
}
