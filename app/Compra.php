<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
/**
* Modelo de la tabla Compras
*/
class Compra extends Model
{
  protected $table = "COMPRAS";
  protected $fillable = array('id_ingrediente','id_orden','cantidad','fecha','ultima');
  protected $guarded = array('id');
  public $timestamps = false;

  public function ingrediente(){
     return $this->belongsTo('App\Ingrediente','id_ingrediente','id');
  }
  public function orden(){
    return $this->belongsTo('App\Orden','id_orden','id');
  }
}
