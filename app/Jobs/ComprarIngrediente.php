<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Compra;
use App\Orden;
use App\Ingrediente;
use App\Job;

class ComprarIngrediente implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orden;
    protected $ingrediente;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id_orden, $id_ingrediente)
    {
        $this->orden = Orden::find($id_orden);
        $this->ingrediente = Ingrediente::find($id_ingrediente);   
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ingrediente = $this->ingrediente;
        $orden = $this->orden;

        //Haciendo la peticion http.
        $client = new Client([
            'base_uri' => 'https://recruitment.alegra.com/api/farmers-market',
        ]);
        $response = $client->get('buy?ingredient='.$ingrediente->nombre);
        $data = json_decode($response->getBody()->getContents());

        if($data->quantitySold > 0){

            //agregando campos de compra
            $compra['id_ingrediente'] = $ingrediente->id;
            $compra['id_orden'] = $orden->id;
            $compra['cantidad'] = $data->quantitySold;
            $compra['fecha'] = Carbon::now()->format('Y/m/d H:m:s');

            //creando compra
            Compra::create($compra);

            //obteniendo todas las compras.
            $compras = Compra::all();

            //verificando que esten todos los ingredientes de la orden
            if($orden->id_estado == 1){
                $actualizar = true;
                foreach ($orden->receta->receta_ingrediente as $ri) {
                    $ing = $ri->ingrediente;
                    if(($ing->cantidad_disp-$ri->cantidad) < 0){

                        //Falta unidades de este ingrediente.
                        $ultima_comp = $orden->compra->where('id_ingrediente',$ing->id)->last();
                        if($ultima_comp && $ultima_comp->ultima){

                            //Si ya se realizo la compra, comprar nuevamente.
                            ComprarIngrediente::dispatch($orden->id,$ing->id);

                            $ultima_comp['ultima'] = false;
                            $ultima_comp->update();
                        }

                        //No cambiar el estado de la orden a entregado
                        $actualizar = false;
                    }
                    
                }
                if($actualizar){

                    //actualizando estado de orden a entregado.
                    $orden['id_estado'] = 2;
                    $orden->update();
                }
            }
            
        }
        else{

            //Intentar comprar nuevamente
            if($orden->id_estado == 1){
                ComprarIngrediente::dispatch($orden->id,$ingrediente->id);
            }   
        }

        //Ver si es el ultimo job
       
        if(Job::all()->count() == 1){
            $ordenes = Orden::all()->where('id_estado',1);
            foreach ($ordenes as $o) {
                /*$ingredientes = [];
                $cont = 0;
                foreach ($o->receta->receta_ingrediente as $ri){
                    $ing = $ri->ingrediente;
                    if(($ing->cantidad_disp-$ri->cantidad) < 0){
                        $ingredientes[$cont++] = $ing;
                    } 
                }*/
                $ingredientes = $o->receta->noDispIngredientes();
                if(count($ingredientes) > 0){
                    //Intentar comprar nuevamente
                    foreach ($ingredientes as $ingre) {
                        ComprarIngrediente::dispatch($o->id,$ingre->id);
                    }
                }
               else{
                    //actualizando estado de orden a entregado.
                    $o['id_estado'] = 2;
                    $o->update();
                }
                
            }
        }
    }
}
