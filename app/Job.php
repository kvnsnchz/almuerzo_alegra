<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
  protected $table = "jobs";
  protected $guarded = array('id');
  public $timestamps = false;

}
