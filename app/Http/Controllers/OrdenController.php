<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Jobs\ComprarIngrediente;
use App\Orden;
use App\Receta;

class OrdenController extends Controller
{
     /**
     * Retorna una vista con la lista 
     * de los pedidos realizados a la cocina
     * junto con su estado (entregado o en preparacion) y las recetas.
     */
    public function index()
    {
        //Obteniendo todas los pedidos hechos a cocina
        $ordenes = Orden::orderBy('fecha','desc')->paginate(10);

        //Obteniendo todas las recetas
        $recetas = Receta::all();

        //Obteniendo la diferencia de fechas
        foreach ($ordenes as $ord) {
            $fecha = new Carbon($ord->fecha);
            $ord['diff_fecha'] = $fecha->diffForHumans(Carbon::now());
        }

        return view('cocina.index',['ordenes'=>$ordenes, 'recetas'=>$recetas]);
    }

    /**
     * Crea un nuevo pedido (orden), con una
     * receta aleatoria, si todos los ingredientes
     * estan disponibles el estado del pedido pasa a ser
     * entregado (id_estado=2), en caso contrario el estado sera 
     * preparando (id_estado=1). 
     */
    public function create()
    {
        //Obtener receta aleatoria
        $receta = Receta::getAleatorio();;
        $orden['id_receta'] = $receta->id;

        //Comprobando disponibilidad de ingredientes
        $ingredientes = $receta->noDispIngredientes();
        $estado = 1;
        if(count($ingredientes) == 0){
            //Si estan todos los ingredientes se entrega la orden
            $estado = 2;
        }  
        $orden['id_estado'] = $estado;
        $orden['fecha'] = Carbon::now();

        //Generando pedido
        $orden = Orden::create($orden);
        $orden = Orden::find($orden->id);

        //Comprar los ingredientes
        if($estado = 1){
            foreach ($ingredientes as $ingrediente) {
                ComprarIngrediente::dispatch($orden->id,$ingrediente->id);
            }
        }

        //Redireccionando a la lista de ordenes
        return redirect()->route('index');
    }
}
