<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receta;
use App\Ingrediente;
use App\Orden;
use App\Compra;

class EstadisticaController extends Controller
{
     /**
     * Retorna una vista con las estadisticas
     */
    public function index()
    {
        //Obteniendo todas los pedidos hechos a cocina
        $recetas = Receta::all();

        //Obteniendo todas las recetas
        $ingredientes = Ingrediente::all();

        //Obteniendo n° de ordenes por cada receta
        $sin_ordenes = (count(Orden::all()) == 0)?true:false;
        for($i = 0; $i < count($recetas); $i++) {
        	$n_ordenes[$i] = count($recetas[$i]->orden);
        }

        //Obteniendo n° de compras por cada ingrediente
        $sin_compras = (count(Compra::all()) == 0)?true:false;
        for($i = 0; $i < count($ingredientes); $i++) {
        	$n_compras[$i] = count($ingredientes[$i]->compra);
        }


        return view('estadistica.index',['recetas'=>$recetas, 
        								'ingredientes'=>$ingredientes,
        								'n_ordenes' => $n_ordenes,
        								'sin_ordenes'=>$sin_ordenes,
        								'n_compras' => $n_compras,
        								'sin_compras'=>$sin_compras,
        								]);
    }
}
