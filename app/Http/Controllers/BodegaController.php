<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Ingrediente;
use App\Compra;

class BodegaController extends Controller
{
    /**
     * Retorna una vista con la lista 
     * de los ingredientes, y las compras.
     */
    public function index()
    {
        //Obteniendo el inventario de ingredientes en bodega
        $ingredientes = Ingrediente::all();

        //Obteniendo las compras realizadas
        $compras = Compra::orderBy('fecha','desc')->paginate(10);

        //Obteniendo la diferencia de fechas en minutos
        foreach ($compras as $comp) {
            $fecha = new Carbon($comp->fecha);
            $comp['diff_fecha'] = $fecha->diffForHumans(Carbon::now());
        }

        return view('bodega.index',['ingredientes'=>$ingredientes, 'compras'=>$compras]);
    }
}
