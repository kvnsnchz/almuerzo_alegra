<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
* Modelo de la tabla Ordenes
*/
class Orden extends Model
{
  protected $table = "ORDENES";
  protected $fillable = array('id_receta','id_estado','fecha');
  protected $guarded = array('id');
  public $timestamps = false;

  public function receta(){
     return $this->belongsTo('App\Receta','id_receta','id');
  }
  public function estado(){
      return $this->belongsTo('App\Estado','id_estado','id');
  }
  public function compra(){
    return $this->hasMany('App\Compra','id_orden','id');
  }
}
