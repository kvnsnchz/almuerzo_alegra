<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
* Modelo de la tabla Recetas_Ingredientes
*/
class Receta_Ingrediente extends Model
{
  protected $table = "RECETAS_INGREDIENTES";
  protected $fillable = array('id_receta','id_ingrediente','cantidad');
  protected $guarded = array('id');
  public $timestamps = false;
  
  public function receta(){
     return $this->belongsTo('App\Receta','id_receta','id');
  }
  public function Ingrediente(){
      return $this->belongsTo('App\Ingrediente','id_ingrediente','id');
  }
}
