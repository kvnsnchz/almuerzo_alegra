<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
* Modelo de la tabla Recetas
*/
class Receta extends Model
{
  protected $table = "RECETAS";
  protected $fillable = array('nombre');
  protected $guarded = array('id');
  public $timestamps = false;
  
  public function receta_ingrediente(){
      return $this->hasMany('App\Receta_Ingrediente','id_receta','id');
  }
  public function orden(){
     return $this->hasMany('App\Orden','id_receta','id');
  }

  /**
  * Retorna una receta aleatoria
  */
  public static function getAleatorio(){
    $id_receta = rand(0,Receta::all()->count()-1);
    return Receta::all()->get($id_receta);
  }

  /**
  * Retorna los ingredientes que no estan disponibles
  */
  public function noDispIngredientes(){
    $ingredientes = [];
    $cont = 0;
    foreach ($this->receta_ingrediente as $ri){
        $ing = $ri->ingrediente;
        if(($ing->cantidad_disp-$ri->cantidad) < 0){
            $ingredientes[$cont++] = $ing;
        }       
    }
    return $ingredientes;
  }
}
