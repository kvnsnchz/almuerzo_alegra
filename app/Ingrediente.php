<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
* Modelo de tabla Ingredientes
*/
class Ingrediente extends Model
{
  protected $table = "INGREDIENTES";
  protected $fillable = array('nombre','cantidad_disp');
  protected $guarded = array('id');
  public $timestamps = false;
  
  public function receta_ingrediente(){
     return $this->hasMany('App\Receta_Ingrediente','id_ingrediente','id');
  }
  public function compra(){
      return $this->hasMany('App\Compra','id_ingrediente','id');
  }
}
