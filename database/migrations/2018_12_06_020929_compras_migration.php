<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComprasMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('COMPRAS', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_ingrediente')->unsigned();
            //Pedido por el cual fue solicitada la compra.
            $table->integer('id_orden')->unsigned();
            $table->integer('cantidad');
            $table->dateTime('fecha');
            //Campo para saber si fue la ultima compra del ingrediente para ese pedido.
            $table->boolean('ultima')->default(true);
            //Foreign Key
            $table->foreign('id_ingrediente')->references('id')->on('INGREDIENTES')->onDelete('cascade');
            $table->foreign('id_orden')->references('id')->on('ORDENES')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('COMPRAS');
    }
}
