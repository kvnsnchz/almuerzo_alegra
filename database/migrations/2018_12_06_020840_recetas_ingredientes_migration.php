<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecetasIngredientesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RECETAS_INGREDIENTES', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_receta')->unsigned();
            $table->integer('id_ingrediente')->unsigned();
            $table->integer('cantidad');

            //Foreign Keys
            $table->foreign('id_receta')->references('id')->on('RECETAS')->onDelete('cascade');
            $table->foreign('id_ingrediente')->references('id')->on('INGREDIENTES')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RECETAS_INGREDIENTES');
    }
}
