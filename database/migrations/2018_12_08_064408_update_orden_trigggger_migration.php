<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdenTriggggerMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Creando trigger para restar de la bodega los ingredientes
        //utilizados para una receta en un pedido.
        DB::unprepared("
        
        CREATE TRIGGER update_orden AFTER UPDATE ON ORDENES
        FOR EACH ROW BEGIN
            UPDATE INGREDIENTES SET cantidad_disp=cantidad_disp-(
                SELECT cantidad from RECETAS_INGREDIENTES 
                WHERE RECETAS_INGREDIENTES.id_receta=NEW.id_receta && 
                RECETAS_INGREDIENTES.id_ingrediente=INGREDIENTES.id)
                WHERE NEW.id_estado=2 && OLD.id_estado=1 && INGREDIENTES.id in (
                SELECT id_ingrediente FROM RECETAS_INGREDIENTES 
                WHERE RECETAS_INGREDIENTES.id_receta=NEW.id_receta);
        END

        ");
    }

}
