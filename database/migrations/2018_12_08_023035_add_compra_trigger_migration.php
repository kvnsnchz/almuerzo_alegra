<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompraTriggerMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Creando trigger para agregar a la bodega los ingredientes comprados.
        DB::unprepared("
        
        CREATE TRIGGER insert_compra AFTER INSERT ON COMPRAS
        FOR EACH ROW BEGIN
            UPDATE INGREDIENTES SET cantidad_disp=cantidad_disp+NEW.cantidad
                WHERE INGREDIENTES.id=NEW.id_ingrediente;
        END

        ");
    }
}
