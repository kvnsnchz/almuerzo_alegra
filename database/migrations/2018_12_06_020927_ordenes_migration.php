<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdenesMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ORDENES', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_receta')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->dateTime('fecha');

            //Foreign Keys
            $table->foreign('id_receta')->references('id')->on('RECETAS')->onDelete('cascade');
            $table->foreign('id_estado')->references('id')->on('ESTADOS')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ORDENES');
    }
}
