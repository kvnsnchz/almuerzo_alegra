<?php

use Illuminate\Database\Seeder;
use App\Ingrediente;
/**
* Seeder para Ingredientes
*/
class IngredienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Agregando los ingredientes,
    	// Con 5 unidades disponibles en bodega para c/u.

        Ingrediente::create
        ([
            'nombre' => 'tomato',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'lemon',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'potato',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'rice',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'ketchup',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'lettuce',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'onion',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'cheese',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'meat',
            'cantidad_disp' => 5,
        ]);

        Ingrediente::create
        ([
            'nombre' => 'chicken',
            'cantidad_disp' => 5,
        ]);
    }
}
