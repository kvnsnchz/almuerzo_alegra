<?php

use Illuminate\Database\Seeder;
use App\Estado;
//Seeder para los Estados de las Ordenes
class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estado::create
        ([
            'nombre' => 'preparando',
        ]);
        Estado::create
        ([
            'nombre' => 'entregado',
        ]);
    }
}
