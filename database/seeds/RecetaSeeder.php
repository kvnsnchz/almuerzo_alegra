<?php

use Illuminate\Database\Seeder;
use App\Receta;
use App\Receta_Ingrediente;
/**
* Seeder para la creación de Recetas
*/
class RecetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creando Recetas

        Receta::create
        ([
            'nombre' => 'receta 1',
        ]);

        Receta::create
        ([
            'nombre' => 'receta 2',
        ]);

        Receta::create
        ([
            'nombre' => 'receta 3',
        ]);

        Receta::create
        ([
            'nombre' => 'receta 4',
        ]);

        Receta::create
        ([
            'nombre' => 'receta 5',
        ]);

        Receta::create
        ([
            'nombre' => 'receta 6',
        ]);
        
        //Agregando ingredientes a la receta 1

        Receta_Ingrediente::create
        ([
        	'id_receta' => 1,
            'id_ingrediente' => 2,
            'cantidad' => 2,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 1,
            'id_ingrediente' => 4,
            'cantidad' => 3,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 1,
            'id_ingrediente' => 9,
            'cantidad' => 1,
        ]);

        //Agregando ingredientes a la receta 2

        Receta_Ingrediente::create
        ([
        	'id_receta' => 2,
            'id_ingrediente' => 1,
            'cantidad' => 2,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 2,
            'id_ingrediente' => 10,
            'cantidad' => 1,
        ]);

        //Agregando ingredientes a la receta 3

        Receta_Ingrediente::create
        ([
        	'id_receta' => 3,
            'id_ingrediente' => 3,
            'cantidad' => 2,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 3,
            'id_ingrediente' => 5,
            'cantidad' => 1,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 3,
            'id_ingrediente' => 9,
            'cantidad' => 1,
        ]);

        //Agregando ingredientes a la receta 4

        Receta_Ingrediente::create
        ([
        	'id_receta' => 4,
            'id_ingrediente' => 6,
            'cantidad' => 1,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 4,
            'id_ingrediente' => 10,
            'cantidad' => 1,
        ]);

        //Agregando ingredientes a la receta 5

        Receta_Ingrediente::create
        ([
        	'id_receta' => 5,
            'id_ingrediente' => 7,
            'cantidad' => 2,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 5,
            'id_ingrediente' => 8,
            'cantidad' => 1,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 5,
            'id_ingrediente' => 9,
            'cantidad' => 1,
        ]);

        //Agregando ingredientes a la receta 6

        Receta_Ingrediente::create
        ([
        	'id_receta' => 6,
            'id_ingrediente' => 2,
            'cantidad' => 3,
        ]);

        Receta_Ingrediente::create
        ([
        	'id_receta' => 6,
            'id_ingrediente' => 10,
            'cantidad' => 1,
        ]);
    }
}

